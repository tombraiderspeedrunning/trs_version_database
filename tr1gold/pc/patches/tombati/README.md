This patch was released by Eidos and is still available under the following link:
https://web.archive.org/web/20081120212648/http://ftp.eidos-france.fr/pub/fr/tomb_raider/patches/tombati.zip

The executable will need to be put into the directory of the tombatiragepro patch and used together with any Tomb Raider 1 Unfinished Business CD.
