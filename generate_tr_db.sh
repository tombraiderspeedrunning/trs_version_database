#!/usr/bin/env bash
#
# This script prints out a database of the files in the current directory,
# recursively
#
# The database that is printed out is tab-separated with the following fields:
# File, Size, Timestamp, MD5 Hash
#
# Example: generate_tr_db.sh > ~/tr_db.csv
#

IFS=$'\n'

# Print path, filesize and MD5 hash, sort by path, convert to lowercase
find . -type f -exec rhash -p '%p\t%s\t%m\n' {} + 2> /tmp/tr_db_err \
	| sort -k 1,1 \
	| sed "s|  ||g" \
	| tr A-Z a-z > /tmp/tr_db_1

# PS1 FMV files can't be properly read for some reason,
# don't calculate MD5 Hash for those
if [ -f /tmp/tr_db_err ]; then
	failed_files=`cat /tmp/tr_db_err | sed -e "s/RHash: \(.*\): .*/\1/g"`

	rm -f /tmp/tr_db_failed

	for f in ${failed_files[@]}
	do
		# Print path and filesize, convert to lowercase
		rhash -p '%p\t%s' "${f}" | tr A-Z a-z >> /tmp/tr_db_failed

		# Add MD5 Hash placeholder
		echo -e "\t###### Input/Output Error ######\n" >> /tmp/tr_db_failed
	done

	# Combine failed ones with successful ones
	cat /tmp/tr_db_1 >> /tmp/tr_db_failed
	cat /tmp/tr_db_failed | sort -k 1,1 > /tmp/tr_db_1
fi

# Print path and modification date,  sort by path, convert to lowercase,
# remove starting ./, remove trailing zeros
find . -type f -exec stat --print='%n\t%y\n' {} + | sort -k 1,1 | tr A-Z a-z | \
	 sed -e 's/\.\///g' -e 's/\.000000000//g' > /tmp/tr_db_2

# Print the database header
echo -e 'File\tSize\tTimestamp\tMD5 Hash'

# Print the database
join -t $'\t' /tmp/tr_db_1 /tmp/tr_db_2 | \
	awk -F  "\t" '{ print $1 "\t" $2 "\t" $4 " " $5 " " $6 "\t" $3 }'

rm -f /tmp/tr_db_*
