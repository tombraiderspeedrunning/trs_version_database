#!/usr/bin/env bash

IFS=$'\n'

mdfiles=`find . -name "*metadata.json" -printf '%p\n' | sort` # only for entries with a disc

clean_toc() {
	# Remove double line feed between entries of TOC file
	sed -i -z "s/\n\n\n\//\n\n\//g" $1

	# Remove empty last line
	if [[ `tail -1 $1 | wc -c` == "1" ]]; then
		sed -i '$d' $1
	fi
}

for f in ${mdfiles}; do
	tocfile="`dirname ${f}`/disc.toc"
	discfile="`dirname ${f}`/disc.csv"
	iffile="`dirname ${f}`/installed_files.csv"

	[ -f ${tocfile} ] && clean_toc ${tocfile}

	if [ -f ${tocfile} ]; then
		disc_label=`grep DATAFILE ${tocfile} | head -1 | sed 's/DATAFILE "//g; s/.bin".*//g'`
		disc_track1_size=`grep -e "length in bytes" ${tocfile} | head -1 | sed 's/^.*length in bytes: //g'`
		disc_tracks=`grep "Track" ${tocfile} | wc -l`
	else
		disc_label=""
		disc_track1_size=""
		disc_tracks=""
	fi

	latest_timestamp=`cat ${discfile} ${iffile} 2> /dev/null \
						| awk -F "\t" -v OFS='\t' '{print $3,$1}' \
						| grep -v Timestamp | grep -v 2018 | grep -v 2019 | grep -v 2020 \
						| sort | tail -1`


	sed -e "s|\(source.* \"\)|source\":           \"|g" \
		-e "s|.*disc_label.*|\t\"disc_label\":       \"${disc_label}\",|g" \
		-e "s|.*disc_track1_size.*|\t\"disc_track1_size\":  ${disc_track1_size},|g" \
		-e "s|.*disc_tracks.*|\t\"disc_tracks\":       ${disc_tracks},|g" \
		-e "s|.*latest_timestamp.*|\t\"latest_timestamp\": \"${latest_timestamp}\"|g" \
		-i ${f}

	echo "${f}:"
	cat ${f}
done
