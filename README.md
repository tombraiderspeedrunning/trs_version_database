Tomb Raider Speedrunning - Version Database
=========================================

This repository contains a database of verified game versions of classic Tomb Raider (1-5) games.
[TRS Discord Server](https://discord.gg/KV6brta)

Disc images and toc files have been created with [cdrdao](http://cdrdao.sourceforge.net/).

## See Also

* [TRS Records](https://gitlab.com/tombraiderspeedrunning/trs_records)
* [TRS Resources](https://gitlab.com/tombraiderspeedrunning/trs_resources)
