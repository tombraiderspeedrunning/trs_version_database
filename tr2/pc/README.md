This directory contains a database of verified game versions of the PC version of Tomb Raider 2.

The game versions are split into different directories based on their game mechanics:
- **Original:** The original release from October/November 1997
- **Botched:** A re-release from December 1997 with gameplay mechanics not found in other TR games
- **Digital:** A version usually found in digital releases (Steam, GOG, etc.) with even weirder gameplay mechanics
