This Original game version was included in an edition of the German Computer Bild Spiele magazine 08/2001 (see pictures/computer_bild_spiele_cd.jpg).
The disc label is "tombraider2".
The disc contains a patcher (patch/tombraider2_patch2.exe) which is a ZIP archive containing a Digital(Multipatch) executable. This patch is not applied automatically and there's no instructions.

Info:
- No FMV disable option
- Behaves like the PS1 versions, e.g. roll corner bugs work, step glitches are easy, etc.
- Needs the CD to run
