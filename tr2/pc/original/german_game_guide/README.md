This Original Tomb Raider 2 version is also known as Soul's (.exe).

The game was included in a version of the german game guide (see pictures/german_game_guide.jpg).
The disc label is "TOMBRAIDER2".

The following directories were deleted from ggg_cd.csv for clarity:
- directx
- manual

Info:
- No FMV disable option
- Behaves like the PS1 versions, e.g. roll corner bugs work, step glitches are easy, etc.
- Needs the CD to run
