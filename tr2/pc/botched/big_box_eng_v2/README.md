This Botched Tomb Raider 2 version is also known as EPC.

This game was first found on CD1 of the English Eidos Premier Collection which contains The Golden Mask on CD2. This game version was also found in a Big Box release in the UK.

The disc label is "1997-12-11-16-53-01-31" and it contains a fixed FLOATING.TR2 and a tomb2.exe that differs from the Original version:

data/floating.tr2  4007486  1997-12-08 16:36:46 +0100 1e7d0d88ff9d569e22982af761bb006b
tomb2.exe           912896  1997-12-11 11:44:22 +0100 df3eeee4a8ed9a3801fd4e569b94a8e5

The tomb2.exe was apparently compiled about 5 hours before the CD went gold. It is assumed it was compiled from some dirty state of the codebase in a rush, hence the name Botched version.

Core/Eidos probably re-released this game version after December 1997, it is found in almost all english physical releases. Older Big Box releases than that may contain the Original version.

The following directories were deleted from EPC_ENG_CD1.csv for clarity:
- directx

Info:
- Has FMV disable option
- Behaves differently than other Tomb Raider games in the series. There's forward momentum when you bump into walls with running speed/standing jump speed. Step bugs are extremely precise to do. QWOP's have different quicksand bug behaviour than in the Original version or TR1/TR3. Fast Flicker doesn't work.
- Needs the CD to run
